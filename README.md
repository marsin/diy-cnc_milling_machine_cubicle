# CNC milling machine cubicle

 ![Cover Image][img_cubicle]


## About

 - Cabinet for the [STEPCRAFT-2/D.600 Construction Kit][lnk_stepcraft].
   - Consisting a 'Wagon' and a 'Cell'
 - Parametric construction made with FreeCAD _(version 0.18)_
   - For changing the parameters have a look at the 'Spreadsheet'

 __ATTENTION__: The construction is in development and can still change strongly!


### Disclaimer

 - The construction comes without any warranty!
 - The construction is neither complete nor suitable for everyone.
   Individual and specific decisions have to be made by yourself
   and also during assembly (e.g. the positioning of the screws).


## CAD Files

 - `Cubicle.fcstd`:          The main project file, includes the 'Wagon' and the 'Cell' _(parametric)_
 - `RubberBuffer.fcstd`:     The rubber buffer to mount the machine on the counter top _(static)_
 - `Caster.fcstd`:           The roller casters for the cubicle _(static, for illustration only)_
 - `Rail.fcstd`:             The rails for the drawers _(static, for illustration only)_
 - `Grip.fcstd`:             The hand grips for the drawers _(static, for illustration only)_
 - `Angle.fcstd`:            The angles for mounting the cell on the wagon _(static)_
 - `HexBolt.fcstd`:          The bolt and nut for mounting the cell on the wagon _(parametric)_
 - `Stepcraft-2_D600.fcstd`: An approximated model of the STEPCRAFT-2/D.600 CNC milling machine _(parametric, for illustration only)_


## Part List

### Machine Wagon

 - 4 rubber buffer, one sided screw (M5 x 10mm)
 - 4 casters (everyone has 4 holes for a M8 screw)
   - 16 M8 x 40 mm screws
   - 16 lock washers for M8
   - 16 big washers for M8
   - 16 small washers for M8
   - 16 self locking screw-nuts
 - 2 pairs of rails for the drawers
   - 6 M4 x 20 mm screws with countersunk heads and screw-nuts for mounting the drawer rails on the side walls
   - 6 M4 x 20 mm pan head screws, screw-nuts and washers for mounting the drawer on the rails
 - 3 hand grips for the second drawer and the two front doors
 - 2 pairs of concealed hinges for the front doors
 - 4 angles for mounting the machine cell
 - About 50 4.5 x 50mm wood screws _(I pre-drilled all screws with 3 mm diameter)_
 - 4 cable hole covers


### Machine Cell

 _TODO_


## Building

 - BottomPlate:
   - Drill and mount the Caster before mounting the Walls. It's easier doing this first.

 - UpperDrawerFrontPlate:
   - Pre-drill the holes for the screws first.
   - Be careful while sawing. With the hole, the plate becomes very unstable.
     It becomes stable again after mounting it on the drawer.

 

   [lnk_stepcraft]: https://shop.stepcraft-systems.com/Buy-D600-CNC-Router-Construction-Kit    "STEPCRAFT-2/D.600"

   [img_cubicle]: ./img/cubicle.png "Assembled Cubicle"

